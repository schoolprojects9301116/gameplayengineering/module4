// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleMovementState.h"

#include "PlatformPuzzlePawn.h"

SimpleMovementState::SimpleMovementState()
{
	_leftMovement = 0;
	_rightMovement = 0;
}

SimpleMovementState::~SimpleMovementState()
{
}

SimpleInputState* SimpleMovementState::HandleInput(APlatformPuzzlePawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	if (actionType == InputActionType::MoveLeft)
	{
		if (inputEvent == IE_Pressed)
		{
			_leftMovement = -1;
		}
		else if (inputEvent == IE_Released)
		{
			_leftMovement = 0;
		}
	}
	else if (actionType == InputActionType::MoveRight)
	{
		if (inputEvent == IE_Pressed)
		{
			_rightMovement = 1;
		}
		else if (inputEvent == IE_Released)
		{
			_rightMovement = 0;
		}
	}

	return nullptr;
}

void SimpleMovementState::EnterState(APlatformPuzzlePawn& pawn)
{}

SimpleInputState* SimpleMovementState::Update(APlatformPuzzlePawn& pawn, float dt)
{
	auto mesh = pawn.GetPlayerMesh();
	if (mesh != nullptr)
	{
		FVector direction(_rightMovement + _leftMovement, 0, 0);
		direction *= pawn.GetSpeed();
		mesh->AddForce(direction);
	}
	return nullptr;
	//pawn.AddActorWorldOffset(direction);
}