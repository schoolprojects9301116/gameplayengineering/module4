// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class APlatformPuzzlePawn;

UENUM()
enum class InputActionType
{
	Jump = 0,
	MoveLeft = 1,
	MoveRight = 2
};

/**
 * 
 */
class PLATFORMPUZZLER_API SimpleInputState
{
public:
	SimpleInputState();
	virtual ~SimpleInputState();
	virtual SimpleInputState* Update(APlatformPuzzlePawn& pawn, float dt) = 0;
	virtual SimpleInputState* HandleInput(APlatformPuzzlePawn& pawn, InputActionType actionType, EInputEvent inputEvent) = 0;
	virtual void EnterState(APlatformPuzzlePawn& pawn) = 0;
};
