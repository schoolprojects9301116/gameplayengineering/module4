// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "SimpleInputState.h"
#include "CoreMinimal.h"

/**
 * 
 */
class PLATFORMPUZZLER_API SimpleJumpingState : public SimpleInputState
{
public:
	SimpleJumpingState();
	virtual ~SimpleJumpingState();

	virtual SimpleInputState* HandleInput(APlatformPuzzlePawn& pawn, InputActionType actionType, EInputEvent inputEvent) override;
	virtual SimpleInputState* Update(APlatformPuzzlePawn& pawn, float dt) override;
	virtual void EnterState(APlatformPuzzlePawn& pawn) override;
};
