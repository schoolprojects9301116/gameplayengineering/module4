// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/BoxComponent.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EndOfLevelTrigger.generated.h"

UCLASS()
class PLATFORMPUZZLER_API AEndOfLevelTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEndOfLevelTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	UBoxComponent* _triggerBox;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _indicator;

	UPROPERTY(EditAnywhere)
	UMaterialInstance* _activeMaterial;

	UPROPERTY(EditAnywhere)
	UMaterialInstance* _inactiveMaterial;

	UPROPERTY(EditInstanceOnly)
	AActor * _targetActor;

	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
			, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
		, const FHitResult& SweepResult);

	UFUNCTION()
	void EndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
		, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
