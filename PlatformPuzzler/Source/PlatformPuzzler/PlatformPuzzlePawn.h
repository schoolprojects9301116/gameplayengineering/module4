// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SimpleInputState.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlatformPuzzlePawn.generated.h"

class SimpleInputState;

UCLASS()
class PLATFORMPUZZLER_API APlatformPuzzlePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlatformPuzzlePawn();

	float GetSpeed() const;
	float GetJumpForce() const;
	float GetRaycastDistance() const;

	UStaticMeshComponent* GetPlayerMesh() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	SimpleInputState* _currentInputState;
	SimpleInputState* _currentJumpState;

	DECLARE_DELEGATE_TwoParams(ForwardInputHandlingDelegate, InputActionType, EInputEvent);

	UFUNCTION()
	void ForwardInputHandling(InputActionType actionType, EInputEvent inputEvent);

	UFUNCTION()
	void ForwardJumpInputHandling(InputActionType actionType, EInputEvent inputEvent);

	UPROPERTY(EditAnywhere)
	float _movementSpeed;

	UPROPERTY(EditAnywhere)
	float _jumpForce;

	UPROPERTY(EditAnywhere)
	float _landedRaycastDistance;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _playerMesh;

	UPROPERTY(EditAnywhere)
	float _maxVelocity;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
