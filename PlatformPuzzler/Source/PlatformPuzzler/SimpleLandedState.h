// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SimpleInputState.h"

/**
 * 
 */
class PLATFORMPUZZLER_API SimpleLandedState : public SimpleInputState
{
public:
	SimpleLandedState();
	virtual ~SimpleLandedState();

	virtual SimpleInputState* HandleInput(APlatformPuzzlePawn& pawn, InputActionType actionType, EInputEvent inputEvent) override;
	virtual SimpleInputState* Update(APlatformPuzzlePawn& pawn, float dt) override;
	virtual void EnterState(APlatformPuzzlePawn& pawn) override;
};
