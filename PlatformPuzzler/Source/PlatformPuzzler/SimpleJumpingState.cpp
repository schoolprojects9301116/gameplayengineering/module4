// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleJumpingState.h"
#include "PlatformPuzzlePawn.h"
#include "SimpleLandedState.h"

SimpleJumpingState::SimpleJumpingState()
{
}

SimpleJumpingState::~SimpleJumpingState()
{
}

SimpleInputState* SimpleJumpingState::HandleInput(APlatformPuzzlePawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	return nullptr;
}

SimpleInputState* SimpleJumpingState::Update(APlatformPuzzlePawn& pawn, float dt)
{
	auto mesh = pawn.GetPlayerMesh();
	if (mesh != nullptr)
	{
		auto world = pawn.GetWorld();
		if (world != nullptr)
		{
			auto targetPoint = pawn.GetActorLocation();
			targetPoint.Z -= pawn.GetRaycastDistance();
			FHitResult result;
			if(world->LineTraceSingleByChannel(result, pawn.GetActorLocation(), targetPoint, ECollisionChannel::ECC_Visibility))
			{
				if (result.bBlockingHit)
				{
					return new SimpleLandedState();
				}
			}
		}
	}
	return nullptr;
}

void SimpleJumpingState::EnterState(APlatformPuzzlePawn& pawn)
{
	auto mesh = pawn.GetPlayerMesh();
	if (mesh != nullptr)
	{
		mesh->AddForce(FVector(0, 0, pawn.GetJumpForce()));
	}
}
