// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PuzzlePlatformerPlayerController.generated.h"

class APlatformPuzzlePawn;

/**
 * 
 */
UCLASS()
class PLATFORMPUZZLER_API APuzzlePlatformerPlayerController : public APlayerController
{
	GENERATED_BODY()

	void AddPlayablePawn(APlatformPuzzlePawn* pawn);

	virtual void SetupInputComponent() override;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void NextPawn();

	TArray<APlatformPuzzlePawn*> _playablePawns;
	int _currentPawnIndex = 0;
	
};
