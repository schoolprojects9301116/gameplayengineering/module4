// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleLandedState.h"
#include "SimpleJumpingState.h"

#include "PlatformPuzzlePawn.h"

SimpleLandedState::SimpleLandedState()
{
}

SimpleLandedState::~SimpleLandedState()
{
}

SimpleInputState* SimpleLandedState::HandleInput(APlatformPuzzlePawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	if (actionType == InputActionType::Jump && inputEvent == IE_Pressed)
	{
		return new SimpleJumpingState();
	}
	return nullptr;
}

SimpleInputState* SimpleLandedState::Update(APlatformPuzzlePawn& pawn, float dt)
{
	return nullptr;
}

void SimpleLandedState::EnterState(APlatformPuzzlePawn& pawn)
{

}