// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlatformPuzzlerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PLATFORMPUZZLER_API APlatformPuzzlerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	APlatformPuzzlerGameModeBase();

	void AddEndOfLevelTrigger();
	void ActivateTrigger();
	void DeactivateTrigger();

protected:
	UPROPERTY(BlueprintReadWrite)
	int _numEndOfLevelTriggers;

	UPROPERTY(BlueprintReadWrite)
	int _numActiveTriggers;
};
