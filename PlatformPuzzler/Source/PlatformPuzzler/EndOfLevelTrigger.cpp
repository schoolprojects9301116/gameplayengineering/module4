// Fill out your copyright notice in the Description page of Project Settings.


#include "EndOfLevelTrigger.h"

#include "PlatformPuzzlerGameModeBase.h"

// Sets default values
AEndOfLevelTrigger::AEndOfLevelTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_triggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	_triggerBox->SetGenerateOverlapEvents(true);
	_triggerBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	_triggerBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

	SetRootComponent(_triggerBox);
	_triggerBox->OnComponentBeginOverlap.AddDynamic(this, &AEndOfLevelTrigger::BeginOverlap);
	_triggerBox->OnComponentEndOverlap.AddDynamic(this, &AEndOfLevelTrigger::EndOverlap);

	_indicator = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Indicator Mesh"));

	_indicator->SetupAttachment(_triggerBox);
}

// Called when the game starts or when spawned
void AEndOfLevelTrigger::BeginPlay()
{
	Super::BeginPlay();
	_indicator->SetMaterial(0, _inactiveMaterial);
	auto gameMode = Cast<APlatformPuzzlerGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode != nullptr)
	{
		gameMode->AddEndOfLevelTrigger();
	}
}

// Called every frame
void AEndOfLevelTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEndOfLevelTrigger::BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
	, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && _targetActor != nullptr && OtherActor == _targetActor)
	{
		_indicator->SetMaterial(0, _activeMaterial);
		auto gameMode = Cast<APlatformPuzzlerGameModeBase>(GetWorld()->GetAuthGameMode());
		if (gameMode != nullptr)
		{
			gameMode->ActivateTrigger();
		}
	}
}

void AEndOfLevelTrigger::EndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor != nullptr && _targetActor != nullptr && OtherActor == _targetActor)
	{
		_indicator->SetMaterial(0, _inactiveMaterial);
		auto gameMode = Cast<APlatformPuzzlerGameModeBase>(GetWorld()->GetAuthGameMode());
		if (gameMode != nullptr)
		{
			gameMode->DeactivateTrigger();
		}
	}
}

