// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SimpleInputState.h"

#include "CoreMinimal.h"

/**
 * 
 */
class PLATFORMPUZZLER_API SimpleMovementState : public SimpleInputState
{
public:
	SimpleMovementState();
	virtual ~SimpleMovementState();

	virtual SimpleInputState* HandleInput(APlatformPuzzlePawn& pawn, InputActionType actionType, EInputEvent inputEvent) override;
	virtual SimpleInputState* Update(APlatformPuzzlePawn& pawn, float dt) override;
	virtual void EnterState(APlatformPuzzlePawn& pawn) override;

protected:
	int _leftMovement;
	int _rightMovement;
};
