// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlatformPuzzler.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PlatformPuzzler, "PlatformPuzzler" );
