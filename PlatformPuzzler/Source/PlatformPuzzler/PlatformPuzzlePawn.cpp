// Fill out your copyright notice in the Description page of Project Settings.


#include "PlatformPuzzlePawn.h"

#include "SimpleInputState.h"
#include "SimpleMovementState.h"
#include "SimpleLandedState.h"

// Sets default values
APlatformPuzzlePawn::APlatformPuzzlePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_movementSpeed = 300000.f;
	_jumpForce = 10000000.f;

	_playerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Player Mesh"));
	_currentInputState = new SimpleMovementState();
	_currentInputState->EnterState(*this);

	_currentJumpState = new SimpleLandedState();

	_maxVelocity = 1000;
}

// Called when the game starts or when spawned
void APlatformPuzzlePawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlatformPuzzlePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (_currentInputState != nullptr)
	{
		auto nextState = _currentInputState->Update(*this, DeltaTime);
		if (nextState != nullptr)
		{
			delete _currentInputState;
			_currentInputState = nextState;
			_currentInputState->EnterState(*this);
		}
	}
	if (_currentJumpState != nullptr)
	{
		auto nextState = _currentJumpState->Update(*this, DeltaTime);
		if (nextState != nullptr)
		{
			delete _currentJumpState;
			_currentJumpState = nextState;
			_currentJumpState->EnterState(*this);
		}
	}
	FVector currentVelocity = _playerMesh->GetPhysicsLinearVelocity();
	FVector clampedVelocity = currentVelocity.GetClampedToMaxSize(_maxVelocity);
	_playerMesh->SetPhysicsLinearVelocity(clampedVelocity);
}

// Called to bind functionality to input
void APlatformPuzzlePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction<ForwardInputHandlingDelegate>("Jump", IE_Pressed, this
		, &APlatformPuzzlePawn::ForwardJumpInputHandling, InputActionType::Jump, IE_Pressed);

	PlayerInputComponent->BindAction<ForwardInputHandlingDelegate>("MoveLeft", IE_Pressed, this
		, &APlatformPuzzlePawn::ForwardInputHandling, InputActionType::MoveLeft, IE_Pressed);
	PlayerInputComponent->BindAction<ForwardInputHandlingDelegate>("MoveLeft", IE_Released, this
		, &APlatformPuzzlePawn::ForwardInputHandling, InputActionType::MoveLeft, IE_Released);

	PlayerInputComponent->BindAction<ForwardInputHandlingDelegate>("MoveRight", IE_Pressed, this
		, &APlatformPuzzlePawn::ForwardInputHandling, InputActionType::MoveRight, IE_Pressed);
	PlayerInputComponent->BindAction<ForwardInputHandlingDelegate>("MoveRight", IE_Released, this
		, &APlatformPuzzlePawn::ForwardInputHandling, InputActionType::MoveRight, IE_Released);
}

void APlatformPuzzlePawn::ForwardInputHandling(InputActionType actionType, EInputEvent inputEvent)
{
	if (_currentInputState != nullptr)
	{
		auto nextInputState = _currentInputState->HandleInput(*this, actionType, inputEvent);
		if (nextInputState != nullptr)
		{
			delete _currentInputState;
			_currentInputState = nextInputState;
			_currentInputState->EnterState(*this);
		}
	}
}

void APlatformPuzzlePawn::ForwardJumpInputHandling(InputActionType actionType, EInputEvent inputEvent)
{
	if (_currentJumpState != nullptr)
	{
		auto nextInputState = _currentJumpState->HandleInput(*this, actionType, inputEvent);
		if (nextInputState != nullptr)
		{
			delete _currentJumpState;
			_currentJumpState = nextInputState;
			_currentJumpState->EnterState(*this);
		}
	}
}

float APlatformPuzzlePawn::GetSpeed() const
{
	return _movementSpeed;
}

float APlatformPuzzlePawn::GetJumpForce() const
{
	return _jumpForce;
}

float APlatformPuzzlePawn::GetRaycastDistance() const
{
	return _landedRaycastDistance;
}

UStaticMeshComponent* APlatformPuzzlePawn::GetPlayerMesh() const
{
	return _playerMesh;
}
