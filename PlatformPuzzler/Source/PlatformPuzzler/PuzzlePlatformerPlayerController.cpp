// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzlePlatformerPlayerController.h"

#include "PlatformPuzzlePawn.h"
#include "Kismet/GameplayStatics.h"

void APuzzlePlatformerPlayerController::AddPlayablePawn(APlatformPuzzlePawn* pawn)
{
	_playablePawns.Add(pawn);
}

void APuzzlePlatformerPlayerController::BeginPlay()
{
	TArray<AActor*> foundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlatformPuzzlePawn::StaticClass(), foundActors);
	for (auto& actor : foundActors)
	{
		auto puzzleActor = Cast<APlatformPuzzlePawn>(actor);
		AddPlayablePawn(puzzleActor);
	}
	_currentPawnIndex = 0;
	if (_playablePawns.Num() > 0)
	{
		Possess(_playablePawns[_currentPawnIndex]);
	}
}

void APuzzlePlatformerPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("NextCharacter", IE_Pressed, this, &APuzzlePlatformerPlayerController::NextPawn);
}


void APuzzlePlatformerPlayerController::NextPawn()
{
	if (_playablePawns.Num() > 0)
	{
		UnPossess();
		_currentPawnIndex++;
		if (_currentPawnIndex >= _playablePawns.Num())
		{
			_currentPawnIndex = 0;
		}
		Possess(_playablePawns[_currentPawnIndex]);
	}
}
