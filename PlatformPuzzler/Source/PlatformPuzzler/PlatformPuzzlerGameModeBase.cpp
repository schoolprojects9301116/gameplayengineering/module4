// Copyright Epic Games, Inc. All Rights Reserved.


#include "PlatformPuzzlerGameModeBase.h"
#include "PlatformPuzzlePawn.h"

APlatformPuzzlerGameModeBase::APlatformPuzzlerGameModeBase()
{
	DefaultPawnClass = APlatformPuzzlePawn::StaticClass();
	_numEndOfLevelTriggers = 0;
}

void APlatformPuzzlerGameModeBase::AddEndOfLevelTrigger()
{
	_numEndOfLevelTriggers++;
}

void APlatformPuzzlerGameModeBase::ActivateTrigger()
{
	_numActiveTriggers++;
}

void APlatformPuzzlerGameModeBase::DeactivateTrigger()
{
	_numActiveTriggers--;
}